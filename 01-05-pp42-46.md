---
author: '五 島　学　編（訳・注釈）'
pandoc-latex-environment:
  center:
  - center
  flushright:
  - flushright
  frchapenv:
  - frchapenv
  frsecbenv:
  - frsecbenv
  frsecenv:
  - frsecenv
  frsubenv:
  - frsubenv
  main:
  - main
  recette:
  - recette
title: 'エスコフィエ『フランス料理の手引き』全注解'
---

<!--EPUB用設定=いったんPandoc.mdに変換後にコメントアウトしてEPUB化すること-->
<!--
\renewcommand{\kenten}[1]{<span class="em-dot">#1</span>}
\renewcommand{\textsc}[1]{<span class="sc">#1</span>}
\renewcommand{\ruby}[2]{<ruby>#1<rt>#2</rt></ruby>}
\setlength{\parindent}{1zw}-->


\index{そす@ソース!いきりすふう@イギリス風---|(}
\index{sauce!anglaise|(}


## イギリス風ソース（温製）[^0104_24]

<!--<div class="frsecenv">Sauces Anglaises Chaudes</div>-->

\index{そす@ソース!いきりすふうおんせい@イギリス風温製---}
\index{いきりすふう@イギリス風!そすおんせい@---ソース（温製）}
\index{sauce@sauce!anglaises chaudes@---s anglaises chaudes}
\index{anglais@anglais(e)!sauces chaudes@Sauces ---es chaudes}

[^0104_24]: この節では初版で31、第二版は33、第三版と第四版で30のレシピ
    が掲載されている。1907年刊の英語版*A Guide to Modern Cookery*でこ
    の節に相当する"Hot English Sauces"には10のレシピしか掲載されていな
    い。対象読者がフランス人であるかイギリス人であるかという違いを意識
    し、ニーズに応えるかたちをとったと考えるのが妥当だろう。それはあく
    までもエスコフィエあるいは共同執筆者の解釈を経た「イギリス風」のソー
    スがほとんどだ。例えば「[ローバックソー
    ス](#roe-buck-sauce)」において[ソース・エスパニョ
    ル](#sauce-espagnole)を用いていること、つまりはエスコフィエ的ソー
    スの体系に組み込まれ得るものであることからも判断がつく。



<div class="recette"><!--beginRecette-->

#### クランベリーソース[^0104_1] {#cranberries-sauce}

<div class="frsubenv">Sauce aux Airelles\hspace{1em}\normalfont(Cranberries-Sauce)</div>

\index{そす@---ソース!くらんへり@クランベリー---}
\index{くらんへり@クランベリー!そす@---ソース}
\index{いきりすふう@イギリス風!くらんへりそす@クランベリーソース}
\index{sauce@sauce!airelles@--- aux Airelles (Cranberries-Sauce)}
\index{airelle@airelle!sauce airelles@Sauce aux Airelles (Cranberries-Sauce)}
\index{anglais@anglais(e)!sauce airelles@Sauce aux Airelles (Cranberries-Sauce)}
\index{cranberry!Cranberries-Sauce}
\index[src]{airelles@Airelles (aux)(Cramberries-Sauce)}
\index[src]{くらんへり@クランベリー}

クランベリー500 gを1 Lの湯で、鍋に蓋をして火をとおす。湯をきって、目の細かい網で裏漉しする。

こうしてできたピュレに茹で汁を適量加えてやや濃度のあるソースの状態にする。好みに応じて砂糖を加える。

このソースには市販品があり、水少々を加えて温めるだけで使える。

……七面鳥のロースト用。

[^0104_1]: 英語のcranberryはツルコケモモ（学名 Vaccinium oxycoccos）。
    フランス語airelles rougesはコケモモ（学名 Vaccinium
    vitis-idaea. L.）非常によく似た近縁種でしばしば混同される。本書で
    もとくに区別されていない。









\atoaki{}

#### アルバートソース[^0104_3] {#albert-sauce}

<div class="frsubenv">Sauce Albert\hspace{1em}\normalfont(Albert-Sauce)</div>

\index{いきりすふう@イギリス風!あるはとそす@アルバートソース}
\index{そす@ソース!あるはと@アルバート---}
\index{あるはと@アルバート!そす@---ソース}
\index{sauce@sauce!albert@--- Albert (Albert-Sauce)}
\index{albert@Albert!sauce@Sauce --- (Albert-Sauce)}
\index{anglais@anglais(e)!sauce albert@Sauce Albert (Albert-Sauce)}
\index[src]{albert@Albert (Albert-Sauce)}
\index[src]{あるはと@アルバート}

すりおろしたレフォール[^0104_4]150 gに[白いコンソ
メ](#consomme-blanc-simple)2 dLを注ぎ、弱火で20分間煮る。

[イギリス式バターソース](#butter-sauce)3 dLと生クリーム2 $\frac{1}{2}$
dL、パンの白い身の部分40 gを加える。強火にかけて煮詰め、木ベラで圧し絞
るようにしながら布で漉す[^0104_5]。卵黄2個を加えてとろみを付け
[^0104_6]、塩1つまみとこしょう少々で味を調える。

仕上げに、マスタード小さじ1杯をヴィネガー大さじ1杯で溶いてから加える。

……牛肉、主としてフィレ肉のブレゼに添える。



[^0104_3]: ザクセン＝コーブルク＝ゴータ公アルバート王配（ヴィクトリア
    女王の夫）（1819〜1861）のこと。女王エリザベス二世の高祖父。本書序
    文p.iiにおいて触れられている料理人エルーイがアルバート王配に仕えて
    いたことがある。なお、本書に掲載されていないが、舌びらめ・アルベー
    ル」という料理がある。しかしながら、これはパリのレストラン、マキシ
    ムズ Maxim's でメートルドテルを務めたアルベール・ブラゼール Albert
    Blazer の名を冠したもので1930年代に創案されたもの。このソースとはまっ
    たく関係がないことに注意。

[^0104_4]: raifort ホースラディッシュ、西洋わさび。

[^0104_5]: 二人で作業すると容易。[ヴルテ](#veloute-ou-sauce-blanche-grasse)訳注参照。

[^0104_6]: このソースの特徴として、イギリスのローストビーフに欠かせな
    いものとされるレフォール（ホースラディッシュ）を用いていることの他
    に、とろみ付けにパンと卵黄を使っている点にも注目すべきだろう。とろ
    み付けの要素としてはきわめて中世料理風と言ってもいい。ただし、中世
    の料理では、パンはこんがりと焼いてから粉末状にし、ヴィネガーなどで
    ふやかし、さらに布で漉してとろみ付けに用いるのが一般
    的だった。パンの白い身の部分をそのまま使えるということは、それだけ
    小麦の精白度合いが高いということでもある。







\atoaki{}

#### アロマティックソース {#aromatic-sauce}

<div class="frsubenv">Sauce aux Aromates\hspace{1em}\normalfont(Aromatic-Sauce)</div>

\index{そす@ソース!あろまていつく@アロマティック---}
\index{いきりすふう@イギリス風!あろまていつくそす@アロマティックソース}
\index{こうそう@香草!あろまていつく@アロマティックソース}

\index{sauce@sauce!aromates@--- aux Aromates (Aromatic-Sauce}
\index{aromate@aromate!sauce@Sauce aux Aromates}
\index{anglais@anglais(e)!sauce aromates@Sauce aux Aromates (Aromatic-Sauce)}
\index[src]{aromates@Aromates (aux)(Aromatic-Sauce)}
\index[src]{あろまていつく@アロマティック}


[コンソメ](#consomme-blanc-simple) $\frac{1}{2}$ Lに、タイム1枝、バジ
ル4 g、サリエット[^0104_7]1 g、マジョラム1 g、セージ1 g、シブレット
[^0104_8]1を刻んだもの1つまみ、エシャロット[^0104_10]2個の[アシェ
\*](#hacher-gls)、ナツメグ少々、大粒のこしょう4粒を入れて、10分間[アンフュゼ\*](#infuser-gls)する。

[シノワ\*](#chinois-gls)で漉し、バターで作った[ブロンドの
ルー](#roux-brun)50 gを入れてとろみを付ける[^0104_12]。数分間沸かして
から、レモン $\frac{1}{2}$個分の搾り汁と、みじん切りにして下茹でしてお
いたセルフイユ[^0104_13]とエストラゴン[^0104_14]計大さじ1杯を加えて仕
上げる[^0104_16]。

……大きな魚まるごと1尾の[ポシェ\*](#pocher-gls)あるいは牛、羊肉の大掛かりな仕立て（ルルヴェ[^0104_15]）に添える。

[^0104_7]: シソ科の香草。サマーセイヴォリー。和名キダチハッカ。

[^0104_8]: ciboulette チャイヴ。アサツキと訳されることもあるが、日本のアサツキとは風味が違うので注意。

[^0104_10]: 玉ねぎによく似ているが小さくて水分量の少ない香味野菜。英語由来
    のシャロットと呼ばれることも。日本の青果マーケットに見られる「エシャ
    レット」は未成熟のらっきょうであってまったく別のもの。



[^0104_12]: 本書第四版ではルーは必ずバターを用いる指示がなされているが、初
    版から第三版までは、バターもしくはグレスドマルミット（コンソメなど
    を作る際に浮いてきた油脂をすくい取って漉したもの）を使うという指示
    だったため、「バターで作った」という記述がこのように残っているレシピ
    が散見される。

[^0104_13]: cerfeuil チャービル。

[^0104_14]: estragon フレンチタラゴン。

[^0104_15]: relevé [ソース・ディプロマット](#sauce-diplomate)訳注参照。

[^0104_16]: このソースで用いられている香草類の種類の多さは特筆に値するだろ
    う。ブラウン系の派生ソースにある[香草ソー
    ス](#sauce-aux-fines-herbes)およびホワイト系派生ソースの[香草ソー
    ス](#sauce-aux-fines-herbes-blanche)と比較されたい。








\atoaki{}

#### バターソース {#butter-sauce}

<div class="frsubenv">Sauce au Beurre à l'anglaise\hspace{1em}\normalfont(Butter Sauce)</div>

\index{いきりすふう@イギリス風!はたそす@バターソース}
\index{そす@ソース!はた@バター--}
\index{はた@バター!そすいきりすふう@---ソース（イギリス風）}
\index{sauce@sauce!beurre anglais@--- au Beurre à l'anglaise (Butter Sauce)}
\index{beurre@beurre!sauce anglaise@Sauce au --- à l'anglaise (Butter Sauce)}
\index{anglais@anglais(e)!sauce beurre@Sauce au Beurre à l'anglaise (Butter Sauce)}
\index[src]{Beurre anglaise@Beurre (au)(Butter-Sauce)}
\index[src]{はたそすいきりす@バターソース（イギリス風）}
  
  
フランスの[ソース・ブール](#sauce-au-beurre)と同様に作るが、より濃
度の高い仕上がりにする点が違う。分量は、バター60 g、小麦粉60 g、1 Lあ
たり塩7 gを加えて沸かした湯 $\frac{3}{4}$ L。レモンの搾り汁5〜6滴、バ
ター200 g。とろみ付け用の卵黄は用いない。







\atoaki{}

#### ケイパーソース {#capers-sauce}

<div class="frsubenv">Sauce aux Câpres\hspace{1em}\normalfont(Capers-Sauce)</div>


\index{いきりすふう@イギリス風!けいは@ケイパーソース}
\index{そす@ソース!けいは@ケイパー---}
\index{けいは@ケイパー!そすいきりすふう@---ソース（イギリス風）}
\index{sauce@sauce!capres anglais@--- aux Câpres (Capers-Sauce)}
\index{capre@câpre!sauce capres anglaise@Sauce aux Câpres (Capers-Sauce)}
\index{anglais@anglais(e)!sauce capres anglais@Sauce aux Câpres (Capers-Sauce)}
\index[src]{capres anglaise@Câpres (aux)(Capers-Sauce)}
\index[src]{けいはいきりす@ケイパー（イギリス風）}





上記の[バターソース](#butter-sauce)1 Lあたり大さじ4杯のケイパーを加えたもの。

……茹でた魚に添える。また、イギリス風[^0104_17]に茹でた仔羊腿肉には欠かせない。

[^0104_17]: à l'anglaise （アロングレーズ）。茹でる（下茹でも含む）場
合には、たんに湯で茹でることを指す。なお、パン粉衣 pané à l'anglaise
（パネアロングレーズ） という場合には、現代の日本でもなじみのある、小
麦粉、溶きほぐした卵、パン粉の順で衣を付けて揚げることを言う。調理法全
体を通しての規則性はなく、あくまでも「イギリス風に由来する」または「イ
ギリス風」を意味する程度の表現なので注意。








\atoaki{}

#### セロリソース {#celery-sauce}

<div class="frsubenv">Sauce au Céleri\hspace{1em}\normalfont(Celery-Sauce)</div>


\index{いきりすふう@イギリス風!せろり@セロリソース}
\index{そす@ソース!せろり@セロリ---}
\index{せろり@セロリ!そすいきりすふう@---ソース（イギリス風）}
\index{sauce@sauce!celeri anglais@--- au Céleri (Celery-Sauce)}
\index{celeri@céleri!sauce anglaise@Sauce au Céleri (Celery-Sauce)}
\index{anglais@anglais(e)!sauce celeri@Sauce au Céleri (Celery-Sauce)}
\index[src]{celeri anglaise@Céleri (au)(Celery-Sauce)}
\index[src]{せろりいきりす@セロリ（イギリス風）}




セロリ6株を掃除して、芯のところだけを使う[^0104_18]。これをソテー鍋に並べ、
[白いコンソメ](#consomme-blanc-simple)をセロリがかぶるまで注ぐ。ブーケガルニとクローブを刺
した玉ねぎ1個を入れ、弱火で加熱する。

セロリの水気をきり、鉢に入れてすり潰す。これを布で漉す。こうして出来た
セロリのピュレと同量の[クリームソース](#cream-sauce)を加える。セロリの
茹で汁を煮詰めたものを大さじ2〜3杯加える。

沸騰しない程度に温め、すぐに提供しない場合は湯煎にかけておく。

……茹でた鶏または鶏のブレゼに添える。

[^0104_18]: 緑色が薄いタイプのセロリは中心部が自然に軟白され、柔らかいので、
    フランス料理でも非常に好まれる。








\atoaki{}

#### ローバックソース[^0104_19] {#roe-buck-sauce}

<div class="frsubenv">Sauce Chevreuil\hspace{1em}\normalfont(Roe-buck Sauce)</div>

\index{いきりすふう@イギリス風!ろはつく@ローバックソース}
\index{そす@ソース!ろはつく@ローバック---}
\index{ろはつく@ローバック!そすいきりすふう@---ソース（イギリス風）}
\index{のろしか@ノロ鹿 see {シュヴルイユ}}
\index{しゆうるいゆ@シュヴルイユ!ろはつくそす@ローバックソース（イギリス風）}
\index{sauce@sauce!chevreuil anglaise@--- Chevreuil (Roe-buck Sauce)}
\index{chevreuil@chevreuil!sauce anglaise@Sauce Chevreuil (Roe-buck Sauce)}
\index{anglais@anglais(e)!sauce chevreuil@Sauce Chevreuil (Roe-buck Sauce)}
\index[src]{chevreuil anglaise@Chevreuil (Roe-buck Sauce)}
\index[src]{ろはつく@ローバック（イギリス風）}




中位の大きさの玉ねぎを1 cm角くらいの[ペイザンヌ\*](#paysanne-gls)に刻み、生ハム80 g
も同様に刻む。これをバターで軽く色付くまで炒める。ブーケガルニを入れ、
ヴィネガー1 $\frac{1}{2}$ dLを注ぐ。ほぼ完全に煮詰める。

[ソース・エスパニョル](#sauce-espagnole)3 dLを注ぎ、15分程弱火にかけて、
[デプイエ\*](#depouiller-gls)する。

15分経ったら、ブーケガルニを取り出し、ポルト酒をグラス 1 杯[^0104_22]と[グロゼ
イユのジュレ](#gelee-de-groseilles-procede-a)大さじ1杯強を加えて仕上げる。

……大型ジビエ肉[^0104_23]の料理に添える。





[^0104_19]: roebuck = chevreuil。

[^0104_22]: 本書における「グラス 1 杯 un verre de 〜（アンヴェールドゥ）」は、約 1 dL = 100 mL。

[^0104_23]: この場合は当然、シュヴルイユの料理だが、フランス料理でシュ
ヴルイユ鹿が時間をかけてマリネしてから調理し、そのマリナード（漬け汁）
もソースに用いるのと比べると非常にシンプルなソースになっている点が興味
深い。








\atoaki{}

#### クリームソース {#cream-sauce}

<div class="frsubenv">Sauce Crème à l'anglaise\hspace{1em}\normalfont(Cream-Sauce)</div>

\index{いきりすふう@イギリス風!くりむ@クリームソース}
\index{そす@ソース!くりむ@クリーム---}
\index{くりむ@クリーム!そすいきりすふう@---ソース（イギリス風）}
\index{sauce@sauce!creme anglaise@--- Crème à l'anglaise (Cream-Sauce)}
\index{creme@crème!sauce anglaise@Sauce Crème à l'anglaise (Cream-Sauce)}
\index{anglais@anglais(e)!sauce creme@Sauce Crème à l'anglaise (Cream-Sauce)}
\index[src]{creme anglaise@Crème à l'anglaise (Cream-Sauce)}
\index[src]{くりむ@クリーム（イギリス風）}





バター100 gと小麦粉60 gで[白いルー](#roux-blanc)を作る。

[白いコンソメ](#consomme-blanc-simple) 7 dLでルーをのばし、マッシュルームのエッセンス 1 dL と生クリーム 2 dLを加える。

火にかけて沸騰させる。小玉ねぎ 1 個とパセリ 1 束を加え、弱火で 15 分程加熱する。提供直前に小玉ねぎとパセリは取り出す。

……仔牛の骨付き背肉の塊[^0104_25]のローストに合わせる。

[^0104_25]: 原文 carré （カレ）。骨付き背肉の塊を上から見たときに正方形であることから。この表現は羊、仔羊、仔牛に一般的に使われる。






\atoaki{}

#### シュリンプソース {#shrimps-sauce}

<div class="frsubenv">Sauce Crevettes à l'anglaise\hspace{1em}\normalfont(Shrimps-Sauce)</div>

\index{いきりすふう@イギリス風!しゆりんふ@シュリンプソース}
\index{そす@ソース!しゆりんふ@シュリンプ---}
\index{くるうえつと@クルヴェット!そすいきりすふう@シュリンプソース（イギリス風）}
\index{sauce@sauce!crevettes anglaise@--- Crevettes à l'---e (Shrimps-Sauce)}
\index{crevette@crevette!sauce anglaise@Sauce Crevettes à l'---e (Shrimps-Sauce)}
\index{anglais@anglais(e)!sauce crevettes@Sauce Crevettes à l'---e (Shrimps-Sauce)}
\index[src]{crevettes anglaise@Crevettes (aux)(Shrimps-Sauce)}
\index[src]{しゆりんふ@シュリンプ（イギリス風）}



カイエンヌ少量を加えて風味を引き締めた[イギリス風バターソー
ス](#butter-sauce)1 Lに、アンチョビエッセンス小さじ1杯と殻を剥いた[ク
ルヴェット\*](#crevettes-gls)の尾の身125 gを加える。

……魚料理用。













\atoaki{}

#### デビルソース {#devilled-sauce}

<div class="frsubenv">Sauce Diable\hspace{1em}\normalfont(Devilled Sauce)</div>


\index{いきりすふう@イギリス風!てひるそす@デビルソース}
\index{そす@ソース!てひる@デビル---}
\index{あくま@悪魔 ⇒ ディアーブル!そすいぎりすふう@デビルソース（イギリス風）}
\index{ていあふる@ディアーブル!てひるそす@デビルソース（イギリス風）}
\index{sauce@sauce!diable anglaise@--- Diable (Devilled Sauce)}
\index{diable@diable!sauce anglaise@Sauce --- (Devilled Sauce)}
\index{anglais@anglais(e)!sauce diable@Sauce Diable (Devilled Sauce)}
\index[src]{diable anglaise@Diable à l'anglaise (Devilled-Sauce)}
\index[src]{てひる@デビル（イギリス風）}


1 $\frac{1}{2}$ dLのヴィネガーにエシャロットの[アシェ\*](#hacher-gls)大さじ1杯強を加えて、
半量になるまで煮詰める。[ソース・エスパニョ
ル](#sauce-espagnole)2 $\frac{1}{2}$ dLとトマトピュレ大さじ 2 杯を加え、5 分間
程火を入れる。

仕上げに、ダービーソース[^0104_27]大さじ1杯とカイエンヌ1つまみ強を加え、[シ
ノワ](#chinois-gls)か布で漉す。

[^0104_27]: 原文 Derby-sauce。1940年代にアメリカで市販されていたのは確認され
    ているが、ここで言及されているのとまったく同じかは不明。なお、初版
    および第二版でこの部分は「ハーヴェイソースとウスターシャーソース各
    大さじ1杯」、第三版では「ハーヴェイソースとエスコフィエソース各大
    さじ1」となっている。「ダービーソース」が当初「エスコフィエソース」
    として商品化された後に何らかの事情により名称変更がなされたという可
    能性も否定できないが、第二版および英語版において[ソース・ディアー
    ブル・エスコフィエ](#sauce-diable-escoffier)および[ソース・ロベー
    ル・エスコフィエ](#sauce-robert-escoffier)、さらに第二版と同年刊の
    英語版のみに掲載されているSauce aux Cerises Escoffierソース・オ・
    スリーズ・エスコフィエのように既にエスコフィエブランドの既製品ソー
    スがあるために、矛盾が生じてしまう。第三版の記述が[ソース・ディアー
    ブル・エスコフィエ](#sauce-diable-escoffier)を意味していると解釈す
    れば矛盾は生じないだろう。ハーヴェイソースについては[ブラウングレ
    イヴィー](#brown-gravy)訳注参照。











\atoaki{}

#### スコッチエッグソース {#scotch-eggs-sauce}

<div class="frsubenv">Sauce Ecossaise\hspace{1em}\normalfont(Scotch eggs Sauce)</div>

\index{いきりすふう@イギリス風!すこつちえつくそす@スコッチエッグソース}
\index{そす@ソース!すこつとらんといきりすふう@スコッチエッグ---}
\index{すこつとらんと@スコットランド!すこつちえつくそす@スコッチエッグソース（イギリス風）}
\index{すこつちえつく@スコッチエッグ!そす@---ソース（イリギス風）}
\index{sauce@sauce!ecossaise@--- Ecossaise (Scotch eggs Sauce)}
\index{scotland@Scotland!sauce ecossaise@Sauce Ecossaise (Scotch eggs Sauce)}
\index{anglais@anglais(e)!sauce eccossaise@Sauce Ecossaise (Scotch eggs Sauce)}
\index[src]{ecossaise@Ecossaise (Scotch eggs Sauce)}
\index[src]{すこつちえつく@スコッチエッグ}


バター60 gと小麦粉30 g、沸かした牛乳4 dLで[ベシャメルソー
ス](#sauce-bechamel)を用意する。味付けは通常どおりにすること。ソースが
沸騰したらすぐに、固茹で卵の白身4個を[エマンセ\*](#emincer-gls)して加える。

提供直前に、茹で卵の黄身を目の粗い漉し器で漉して混ぜ込む。

……\ruby{鱈}{たら}には欠かせないソース。










\atoaki{}

#### フェンネルソース[^0104_30] {#fennel-sauce}

<div class="frsubenv">Sauce au Fenouil\hspace{1em}\normalfont(Fennel Sauce)</div>

\index{いきりすふう@イギリス風!ふえんねる@フェンネルソース}
\index{そす@ソース!ふえんねるいきりすふう@フェンネル---（イギリス風）}
\index{ふえんねる@フェンネル!そすいきりすふう@フェンネルソース（イギリス風）}
\index{sauce@sauce!fenouil anglaise@--- au Fenouil (Fennel Sauce)}
\index{fenouil@fenouil!sauce anglaise@Sauce au Fenouil (Fennel Sauce)}
\index{anglais@anglais(e)!sauce fenouil@Sauce au Fenouil (Fennel Sauce)}
\index[src]{fenouil anglaise@Fenouil (Fennel Sauce)}
\index[src]{ふえんねるいきりす@フェンネル}



通常どおりに作った[バターソース](#butter-sauce)2 $\frac{1}{2}$ dLあたり、細かく刻
んで下茹でしたフェンネル大さじ1杯を加える。

……このソースは主として、グリルあるいは茹でた鯖に合わせる。

[^0104_30]: 日本語でフェンネルと呼ばれるものは、1° 主に香草として葉を利用
    するタイプfenouil sauvage（フヌイユソヴァージュ）と、2° 白く肥大し
    た株元を食用とするフローレンス・フェンネルfenouil de florence（フ
    ヌイユ・ド・フロロンス）またはfenouil bulbeux（フヌイユビュルブー）
    と呼ばれる2種がある。本書ではどちらを用いるのか明記されていないこ
    とが多いが、一般に、葉を利用するタイプは香りが非常に強く、フローレ
    ンスフェンネルの葉も食用可能だが、香りは比較的おとなしい。









\atoaki{}

#### グーズベリーソース {#gooseberry-sauce}

<div class="frsubenv">Sauce aux Groseilles\hspace{1em}\normalfont(Gooseberry Sauce)</div>

\index{いきりすふう@イギリス風!くすへりそす@グーズベリーソース}
\index{そす@ソース!くすへりいきりすふう@グーズベリー---}
\index{くすへり@グーズベリー!そすいきりすふう@グーズベリーソース（イギリス風）}
\index{すくり@すぐり!くすへりそすいきりすふう@グーズベリーソース（イギリス風）}
\index{くろせいゆ@グロゼイユ!くすへりそすいきりすふう@グーズベリーソース（イギリス風）}
\index{sauce@sauce!groseilles anglaise@--- aux Groseilles (Gooseberry Sauce)}
\index{groseille@groseille!sauce anglaise@Sauce aux Groseilles (Gooseberry Sauce)}
\index{anglais@anglais(e)!sauce groseilles@Sauce aux Groseilles (Gooseberry Sauce)}
\index[src]{groseilles@groseilles (Gooseberry Sauce)}
\index[src]{くすへり@グーズベリー}





グーズベリー[^3] 1 Lの皮を剥いて洗い、砂糖 125 gと水 1 dLを加えて火にかける。
目の細かい漉し器で裏漉しする。

……このピュレはグリルした鯖に合わせる。


[^3]: フランスでは groseilles à maquereaux （グロゼイユザマクロー）と呼ばれる大粒で緑色のすぐり。






\atoaki{}

#### ロブスターソース {#lobster-sauce}

<div class="frsubenv">Sauce Homard à l'anglaise\hspace{1em}\normalfont(Lobster Sauce)</div>

\index{いきりすふう@イギリス風!ろふすたそす@ロブスターソース}
\index{そす@ソース!ろふすたいきりすふう@ロブスター---}
\index{ろふすた@ロブスター!そすいきりすふう@ロブスターソース（イギリス風）}
\index{おまる@オマール!ろふすたそす@ロブスターソース（イギリス風）}
\index{sauce@sauce!homard anglaise@--- Homard à l'anglaise (Lobster Sauce)}
\index{homard@homard!sauce anglaise@Sauce --- à l'anglaise (Lobster Sauce)}
\index{anglais@anglais(e)!sauce homard@Sauce Homard à l'---e (Lobster Sauce)}
\index[src]{homard anglaise@Homard à l'anglaise}
\index[src]{ろふすた@ロブスター}



カイエンヌを加えて風味を引き締めた[ベシャメルソース](#sauce-bechamel)1
Lに、アンチョビエッセンス大さじ1杯と、[デ\*](#des-gls)に切ったオマールの尾の身
100 gを加える[^0104_31]。

……魚料理用。

[^0104_31]: ホワイト系派生ソースの節にある[ソース・オマール](#sauce-homard)
    を比較すると、このソースのシンプルさが際立って見えるが、ベシャメル
    を基本ソースにしている点で、やはりエスコフィエの「ソースの体系」に組込まれたもの
    であり、純粋にイギリス料理由来というわけでもないと思われる。なお、
    このレシピは初版からほぼ異同がなく、1907年の英語版には含まれていな
    い。









\atoaki{}

#### 牡蠣入りソース {#oyster-sauce}

<div class="frsubenv">Sauce aux Huîtres\hspace{1em}\normalfont(Oyster Sauce)</div>

\index{いきりすふう@イギリス風!かきいりそす@牡蠣入りソース}
\index{そす@ソース!かきいりいきりすふう@牡蠣入り---（イギリス風）}
\index{かき@牡蠣!そすいきりすふう@牡蠣入りソース（イギリス風）}
\index{sauce@sauce!huitres anglaise@--- aux huitres (Oyster Sauce)}
\index{huitre@huître!sauce anglaise@Sauce aux ---s (Oyster Sauce)}
\index{anglais@anglais(e)!sauce huitre@Sauce aux Huîtres (Oyster Sauce)}
\index[src]{huitres anglaise@Huîtres (aux)(Oyster Sauce)}
\index[src]{かきいきりす@牡蠣入り（イギリス風）}



バター20 gと小麦粉15 gで[ブロンドのルー](#roux-blond)を作る。

このルーを、牛乳1 dLと生クリーム1 dLで溶く。塩1つまみを加えて調味し、
火にかけて沸騰させたら弱火にして10分間煮る。

布で漉し、カイエンヌを加えて風味を引き締める。[ポシェ\*](#pocher-gls)
して周囲をきれいに掃除した牡蠣の身12個を1 cm程度の厚さに切って、ソース
に加える。

……もっぱら茹でた魚[^0104_32]に添える。

[^0104_32]: 初版および第二版では「もっぱら茹でた生鱈に合わせる」とある。こ
    のレシピも1907年の英語版には掲載されていない。







\atoaki{}

#### 牡蠣入りブラウンソース {#brown-oyster-sauce}

<div class="frsubenv">Sauce brune aux Huîtres\hspace{1em}\normalfont(Brown Oyster Sauce)</div>

\index{いきりすふう@イギリス風!かきいりふらうんそす@牡蠣入りブラウンソース}
\index{そす@ソース!かきいりふらうんいきりすふう@牡蠣入りブラウン---（イギリス風）}
\index{かき@牡蠣!そすふらうんいきりすふう@牡蠣入りブラウンソース（イギリス風）}
\index{sauce@sauce!brune huitres anglaise@--- brune aux huitres (Brown Oyster Sauce)}
\index{huitre@huître!sauce brune anglaise@Sauce brune aux ---s (Brown Oyster Sauce)}
\index{anglais@anglais(e)!sauce brune huitre@Sauce brune aux Huîtres (Brown Oyster Sauce)}
\index[src]{huitres brune anglaise@Brune aux huîtres (Brown Oyster Sauce)}
\index[src]{かきいりふらうんいきりす@牡蠣入りブラウン（イギリス風）}



上記の牡蠣入りソースと作り方はまったく同じだが、牛乳と生クリームではな
く、[茶色いフォン](#fonds-brun)2 dLを使うこと。

……このソースは、グリル焼きした肉や、肉のプディング[^0104_33]、生鱈のグリル焼きに合わせる。

[^0104_33]: 本書にはイギリス風の肉料理としてのプディングのレシピも掲載され
    ている。[ビーフステークのプディング](#beefteak-pudding)、[ビーフ
    ステークとキドニーのプディング](#beefsteak-and-kidney-pudding)、
    [ビーフステークと牡蠣のプディング](#beefsteak-and-oysters-pudding)。
    なお、本書でのbeefsteakビーフステークとは肉の切り方のことを意味し
    ており、グリル焼きあるいはソテーしたもののことではない。ここでは厚
    さ 1 cm程度にスライスした牛肉のことを指している。










\atoaki{}

#### ブラウングレイヴィー {#brown-gravy}

<div class="frsubenv">Jus coloré\hspace{1em}\normalfont(Brown Gravy)</div>


\index{いきりすふう@イギリス風!ふらうんくれいうい@ブラウングレイビヴィー}
\index{そす@ソース!ふらうんくれいうい@ブラウングレイヴィー（イギリス風）}
\index{くれいうい@グレイヴィー!そすふらうんいきりすふう@ブラウングレイヴィー（イギリス風ソース）}
\index{sauce@sauce!jus colore anglaise@Jus coloré (Brown Gravy)}
\index{gravy@gravy!jus colore anglaise@Jus coloré (Brown Gravy)}
\index{anglais@anglais(e)!jus colore@Jus coloré (Brown Gravy)}
\index[src]{jus colore anglaise@Jus coloré (Brown Gravy)}
\index[src]{ふらうんくれいうい@ブラウングレイヴィー}


[イギリス風バターソース](#butter-sauce)4 dLに、ローストの肉汁 2 dLとケ
チャップ[^0104_34]大さじ $\frac{1}{2}$杯、ハーヴェイソース[^0104_35]大さじ $\frac{1}{2}$杯
を加える。

……もっぱら仔牛のローストに添える。

[^0104_34]: ここではマッシュルームケチャップのこと。マッシュルームの薄切り
    を塩、こしょう、香辛料で5〜6日漬け込み、その絞り汁を沸かして香辛料
    とトマトを加えて味を調え、漉してから保存する (『ラルース・ガストロ
    ノミック』初版)。なお、ketchupは語源が、中国福建省アモイの方言で、
    香辛料を加えて醗酵させた魚醤の一種を意味するのkôe-chiapまたは
    kê-chiap（鮭汁）だとされている。これがマレー語に伝播し、kecap（発
    音はケーチャプ）と変化し、17世紀頃、現在のシンガポールおよびマレー
    シアを植民地支配していたイギリス人の知るところとなった。イギリスに
    も古くから魚醤の類はあり、そのバリエーションのひとつとして、マッシュ
    ルームとエシャロットを添加した魚醤をketchupと呼ぶようになった。や
    がて魚醤文化の衰退とともに、ケチャップと呼ばれるものはマッシュルー
    ムが主原料となり、いわゆるマッシュルームケチャップが18世紀頃に成立
    したとされる。これは、塩漬けにして醗酵させたマッシュルームの搾り汁
    にメース、ナツメグ、こしょうなどの香辛料を加えて煮詰め、漉したもの。
    これにトマトを添加するようになった時期は判然としないが、おそらくは
    19世紀初頭だったと思われる。フランスの料理書では1814年刊ボヴィリエ
    『調理技術』第1巻に作り方が詳述されているが(p.72)、トマトは用いな
    いマッシュルームケチャップのバリエーション。トマトを主原料としたケ
    チャップは、アメリカのハインツHeinzが1876年にハインツ・トマトケチャッ
    プを製品化して以降、徐々に広まっていった。このため、英語圏で成立、
    普及したトマトケチャップがフランスにおいて知られるようになるのは、
    少なくとも上記『ラルース・ガストロノミック』初版（1938年）よりも後
    のことであり、おそらくは第二次大戦後だろうと思われる。


[^0104_35]: Herwey Sauce 19世紀〜20世紀前半にかけて既製品が流通していた。現
    在は商品としては存在していないと思われる。原料はアンチョビ、ヴィネ
    ガー、マッシュルームケチャップ、にんにく、大豆由来原料（詳細不明、
    おそらくは大豆レシチンすなわち大豆油かと思われる）、カイエンヌ、コ
    チニール色素などであったという。








\atoaki{}

#### エッグソース {#eggs-sauce}

<div class="frsubenv">Sauce aux Œufs à l'anglaise\hspace{1em}\normalfont(Eggs Sauce)</div>

\index{いきりすふう@イギリス風!えつくそす@エッグソース}
\index{そす@ソース!えつくそす@エッグ---}
\index{たまこ@卵!えつくいきりすふう@エッグソース（イギリス風）}
\index{sauce@sauce!oeufs anglaise@--- aux ŒEufs à l'anglaise (Eggs Sauce)}
\index{oeufs@œufs!sauce anglaise@Sauce aux --- à l'anglaise (Eggs Sauce)}
\index{anglais@anglais(e)!sauce oeufs@Sauce aux Œufs à l'---e (Eggs Sauce)}
\index[src]{oeufs anglaise@Œufs à l'anglaise (aux)(Egg Sauce)}
\index[src]{えつくそす@エッグ（イギリス風）}

小麦粉60 gとバター30 gで[白いルー](#roux-blanc)を作る。あらかじめ沸か
しておいた牛乳 $\frac{1}{2}$ Lで溶く。塩、白こしょう、ナツメグ少々で味を調
える。火にかけて沸騰したら弱火にして5〜6分間煮る。

固茹で卵2個を白身、黄身ともに、さいの目に刻んでソースに加える。

……ハドック[^0104_36]やモリュ[^0104_37]の料理に合わせるのが一般的。

[^0104_36]: Haddock 鱈の一種。フランス語では同じ綴りでアドックまたは
    églefin, aiglefinエーグルファンと呼ばれる。イギリスでは主に塩漬け
    を燻製にしたものを指す。

[^0104_37]: morue モリュ。干し鱈、塩鱈のこと。生のものはcabillaudカビヨと呼ばれる。









\atoaki{}

#### エッグアンドバターソース {#eggs-and-butter-sauce}

<div class="frsubenv">Sauce aux Œufs au beurre à l'anglaise\hspace{1em}\normalfont(Eggs and Butter Sauce)</div>


\index{いきりすふう@イギリス風!えつくあんとはたそす@エッグアンドバターソース}
\index{そす@ソース!えつくあんとはたそす@エッグアンドバターソース}
\index{はた@バター!えつくあんとはたそす@エッグアンドバターソース（イギリス風）}
\index{sauce@sauce!oeufs beurre fondu anglaise@--- aux Œufs au Beurre fondu (Eggs and butter Sauce)}
\index{oeufs@œufs!sauce beurre fondu@Sauce aux Œufs au beurre fondu (Eggs and butter Sauce)}
\index{anglais@anglais(e)!sauce oeufs beurre fondu@Sauce aux Œufs au beurre fondue (Eggs and butter Sauce)}
\index[src]{oeufs beurre fondu anglaise@Œufs au Beurre fondu (aux)(Eggs and Butter Sauce)}
\index[src]{えつくあんとはたそす@エッグアンドバター（イギリス風）}


バター 250 g を溶かし、塩適量、こしょう少々、レモン $\frac{1}{2}$ 個分の搾り汁、
固茹で卵 3 個を熱いうちに大きめのさいの目に刻んだもの、[アシェ\*](#hacher-gls)して
下茹でしたパセリ小さじ1杯を加える。

……茹でた魚の大きな仕立ての料理[^0104_38]に添える。

[^0104_38]: relevé （ルルヴェ）。\protect\hyperlink{releve}{第二版序文
    訳注} \[p.v\]、および[ソース・ディプロマット](#sauce-diplomate)訳
    注参照。










\atoaki{}

#### オニオンソース {#onions-sauce}

<div class="frsubenv">Sauce aux Oignons\hspace{1em}\normalfont(Onions Sauce)</div>

\index{いきりすふう@イギリス風!おにおんそす@オニオンソース}
\index{そす@ソース!おにおんそす@オニオンソース（イギリス風）}
\index{たまねき@玉ねぎ!そすおにおんいきりすふう@オニオンソース（イギリス風）}
\index{sauce@sauce!oignons anglaise@--- aux Oignons (Onions Sauce)}
\index{oignon@oignon!sauce anglaise@Sauce aux Oignons (Onions Sauce)}
\index{anglais@anglais(e)!sauce oignons@Sauce aux Oignons (Onions Sauce)}
\index[src]{oignons anglaise@Oignons (aux)(anglaise)}
\index[src]{おにおんそす@オニオン（イギリス風）!}

玉ねぎ 200 g を[エマンセ\*](#emincer-gls)する。牛乳 6 dL に塩、こしょう、ナツメグを加え
て玉ねぎを茹でる。

火が通ったらすぐに、玉ねぎの水気をしっかりきって、みじん切りにする。

バター 40 g と小麦粉 40 g で[白いルー](#roux-blanc)を作る。これを玉ねぎを
茹でた牛乳でのばす。火にかけて沸騰させ、みじん切りにした玉ねぎを加える。
ソースはとても濃い状態になっていること。そのまま7〜8分煮る。

……このソースは何にでも合わせられる。うさぎ、鶏、牛などの胃や腸の料理
[^0104_40]、茹でたマトン、ジビエのブレゼなど……このソースは必ず合わせる肉
の上にかけてやること[^0104_41]。



[^0104_40]: tripes トリップ。主として\ruby{反芻}{はんすう}動物（すなわち牛）の胃腸の食材とし
    ての総称。日本ではTripes à la mode de Caen（トリップアラモードドコ
    ン）「カン風トリップ煮込み」が有名。

[^0104_41]: 本書におけるソースは特に指示がない場合はソース入れ（saucière ソ
    シエール）で料理本体と別添して供すると考えておくといい。











\atoaki{}

#### ブレッドソース {#bread-sauce}

<div class="frsubenv">Sauce au Pain\hspace{1em}\normalfont(Bread Sauce)</div>


\index{いきりすふう@イギリス風!ふれつとそす@ブレッドソース}
\index{そす@ソース!ふれつとそす@ブレッドソース}
\index{はん@パン!そすふれつといきりすふう@ブレッドソース（イギリス風）}
\index{sauce@sauce!pain anglaise@--- au Pain (Bread Sauce)}
\index{pain@pain!sauce anglaise@Sauce au Pain (Bread Sauce)}
\index{anglais@anglais(e)!sauce pain@Sauce au Pain (Bread Sauce)}
\index[src]{pain anglaise@Pain (au)(Bread Sauce)}
\index[src]{ふれつとそす@ブレッド（イギリス風）}




牛乳 $\frac{1}{2}$ Lを沸かし、フレッシュなパンの白い身80 gを投入する。塩1つ
まみ強、クローブ1本を刺した小玉ねぎ1個[^1]、バター30 gを加える。

弱火で15分程煮る。玉ねぎを取り出し、泡立て器でソースが滑かになるまでよ
く混ぜる。生クリーム約1 dLを加えて仕上げる。

……鶏やジビエ（鳥類）のローストに合わせる。

###### 【原注】{#nota-bread-sauce}

このブレッドソースを鶏のローストに添える場合は、ローストの肉汁もソース
入れで添えること。ジビエの場合はさらに、よく乾かしたパンを揚げた「ブレッ
ドクランプス」をソース入れに入れて添えること。また、フライドポテトの皿
も添えること。




[^1]: [ソース・カノティエール](#sauce-canotiere)訳注参照。




\atoaki{}

#### フライドブレッドソース {#fried-bread-sauce}

<div class="frsubenv">Sauce au Pain frit\hspace{1em}\normalfont(Fried bread Sauce)</div>


\index{いきりすふう@イギリス風!ふらいとふれつとそす@フライドブレッドソース}
\index{そす@ソース!ふらいとふれつとそす@フライドブレッドソース}
\index{はん@パン!そすふらいとふれつといきりすふう@フライドブレッドソース（イギリス風）}
\index{sauce@sauce!pain frit anglaise@--- au Pain frit (Fried bread Sauce)}
\index{pain@pain!sauce frit anglaise@Sauce au Pain frit (Fried bread Sauce)}
\index{anglais@anglais(e)!sauce pain frit@Sauce au Pain frit (Fried bread Sauce)}
\index[src]{pain frit anglaise@Pain frit (au)(Fried Bread Sauce)}
\index[src]{ふらいとふれつとそす@フライドブレッド}


[コンソメ](#consomme-blanc-simple)2 dLに、小さなさいの目に切った脂身の
ないハム80 gとエシャロット[^2]2個の[アシェ\*](#hacher-gls)を加える。弱火で10分間煮る。

その間に、バター 50 gを熱してパンの身 50 g を揚げておく。提供直前に、揚げ
たパンをコンソメに入れる。パセリの[アシェ\*](#hacher-gls)1 つまみとレモンの搾り汁少々
で仕上げる。

……このレシピは小鳥[^0104_43]のロースト用。

[^2]: フランス料理で好んで使われる香味野菜。玉ねぎと似ているが香り、風
    味がかなり異なる。「エシャレット」の名で流通しているものの多くは未
    成熟の「らっきょう」なので注意。代用にはならない。

[^0104_43]: つぐみ（grive グリーヴ）など小さな鳥類のローストは、下処理し
    た後に胸肉の部分を豚背脂のシートで一羽ずつ包み、数羽をまとめて串刺しにしてロー
    ストするのが一般的だった。








\atoaki{}

#### パセリソース {#perseley-sauce}

<div class="frsubenv">Sauce Persil\hspace{1em}\normalfont(Persley Sauce)</div>


\index{いきりすふう@イギリス風!はせりそす@パセリソース}
\index{そす@ソース!はせりそす@パセリソース}
\index{はせり@パセリ!そすはせりいきりすふう@パセリソース（イギリス風）}
\index{sauce@sauce!persil anglaise@--- Persil (Perseley Sauce)}
\index{persil@persil!sauce anglaise@Sauce Persil (Perseley Sauce)}
\index{anglais@anglais(e)!sauce persil@Sauce Persil (Perseley Sauce)}
\index[src]{persil anglaise@Persil (Persley Sauce)}
\index[src]{はせりそす@パセリ（イギリス風）}




[イギリス風バターソース](#bread-sauce) $\frac{1}{2}$ Lに、パセリをアンフュゼした湯[^0104_44]1 dLを加える。[アシェ\*](#hacher-gls)し、[ブロンシール\*](#blanchir-gls)したパセリの葉大さ
じ1杯強を加えて仕上げる。

……仔牛の頭肉、仔牛の足、脳などに合わせる。

[^0104_44]: infusion アンフュジオン < infuser アンフュゼ（煎じる、香りなど
    を煮出す）。なお、いわゆるハーブティは thé（テ）よりもむしろ、infusion
    と呼ばれるのが一般的。







\atoaki{}

#### 魚料理用パセリソース[^0104_45bis]

<div class="frsubenv">Sauce Persil pour Poissons</div>

\index{いきりすふう@イギリス風!はせりそすさかなりようりよう@パセリソース（魚料理用）}
\index{そす@ソース!さかなりようりようはせりそす@魚料理用パセリソース}
\index{はせり@パセリ!そすはせりいきりすふうさかなりようりよう@パセリソース（魚料理用、イギリス風）}
\index{sauce@sauce!persil poissons anglais@--- Persil pour Poissons}
\index{persil@persil!sauce anglaise poissons@Sauce Persil pour Poissons}
\index{anglais@anglais(e)!sauce persil poissons@Sauce Persil pour Poissons}
\index[src]{persil poissons anglaise@Persil pour Poissons (anglaise)}
\index[src]{さかなりようりようはせりそす@魚料理用パセリ}



[白いルー](#roux-blanc)60 gを、このソースを合わせる魚に火を通すのに使っ
たクールブイヨン $\frac{1}{2}$ Lでのばす。クールブイヨンはパセリの香り
をしっかり効かせたものであること。そうでない場合は、パセリの香りを煮出
した湯を加えてこのソースの特徴をきちんと出してやること。

5〜6 分間加熱する。細かく[アシェ\*](#hacher-gls)し、[ブロンシール
\*](#blanchir-gls)したパセリの葉大さじ1杯とレモン果汁少々で仕上げる。

[^0104_45bis]: このソースは第二版から。英語名は付されていない。






\atoaki{}

#### アップルソース {#apple-sauce}

<div class="frsubenv">Sauce aux pommes\hspace{1em}\normalfont(Apple Sauce)</div>

\index{いきりすふう@イギリス風!あつふるそす@アップルソース}
\index{そす@ソース!あつふるそす@アップルソース}
\index{りんこ@リンゴ!あつふるそす@アップルソース（イギリス風）}
\index{sauce@sauce!pommes anglaise@ aux Pommes (Apple Sauce)}
\index{pomme@pomme!sauce anglaise@Sauce aux Pommes (Apple Sauce)}
\index{anglais@anglais(e)!sauce pommes@Sauce aux Pommes (Apple Sauce)}
\index[src]{pomme anglaise@Pomme (aux)(Apple Sauce)}
\index[src]{あつふるそす@アップル（イギリス風）}



通常どおりにリンゴのマーマレードを作る。砂糖ごく少なめにし、シナモンの粉末を
ほんの少量加えること。……これを提供直前に泡立て器で滑らかになるまでよ
く混ぜる。

……このマーマレードは\ruby{微温}{ぬる}い温度で供する。鴨、がちょう、豚のローストなど、何にでも合う。

###### 【原注】 {#nota-apple-sauce}

ある種のローストにこのマーマレードを添えるというのは、とくにイギリスに
限ったことではない。ドイツ、ベルギー、オランダでも同様に行なわれている。

これらの国では、ジビエのローストにはりんごかこけもものマーマレード、あ
るいは果物のコンポート（冷製、温製どちらも）のいずれかを必ず添える[^0104_46]。

[^0104_46]: 果物のコンポートへの言及は第三版から。また、1907年の英語版*A
    Guide to Modern Cookery*には原注そのものがない。英語版のレシピは
    「中位の大きさのリンゴ 2 ポンド（約900 g）を四つ割りにして皮を剥き、
    芯を取り除いて刻む。これをシチュー鍋に入れ、大さじ1杯の砂糖とシナ
    モン少々、水を大さじ 2〜3 杯加える。蓋をして弱火にかけて煮る。提供直
    前に泡立て器で滑らかにする。このソースは微温い温度で、鴨、がちょう、
    うさぎのローストなどに添える」(p.45)となっている。








\atoaki{}

#### ポートワインソース {#porto-wine-sauce}

<div class="frsubenv">Sauce au Porto\hspace{1em}\normalfont(Porto Wine Sauce)</div>

\index{いきりすふう@イギリス風!ほとわいんそす@ポートワインソース}
\index{そす@ソース!ほとわいんそす@ポートワインソース}
\index{ほると@ポルト!ほとわいんそす@ポートワインソース（イギリス風）}
\index{sauce@sauce!porto anglaise@--- au Porto (Port Wine Sauce)}
\index{porto@porto!sauce anglaise@Sauce au --- (Porto Wine Sauce)}
\index{anglais@anglais(e)!sauce porto@Sauce au Porto (Porto Wine Sauce)}
\index[src]{porto@Porto (au)(Port Wine Sauce)}
\index[src]{ほとわいんそす@ポートワイン（イギリス風）}



ポルト酒1 $\frac{1}{2}$ dLにエシャロットの[アシェ\*](hacher-gls)大さじ1杯とタイム1枝を
加えて半量になるまで煮詰める。オレンジ2個とレモン $\frac{1}{2}$ 個の搾り汁を
加える。器具でおろしたオレンジの[ゼスト\*](#zeste-gls)小さじ1杯と塩
1つまみ、カイエンヌごく少量を加える。

これを布で\ruby{漉}{こ}し、美味しい[とろみを付けた仔牛のジュ](#jus-de-veau-lie)5 dLを加える。


……野生の鴨、その他のジビエ全般に合わせる。

###### 【原注】 {#nota-porto-wine-sauce}

このイギリス料理のソースは、フランスの多くの飲食店で使われている。







\atoaki{}

#### ホースラディッシュソース {#horse-radish-sauce}

<div class="frsubenv">Sauce Raifort chaude\hspace{1em}\normalfont(Horse radish Sauce)</div>

\index{いきりすふう@イギリス風!ほすらていつしゆそす@ホースラディッシュソース}
\index{そす@ソース!ほすらていつしゆそす@ホースラディッシュソース}
\index{れふおる@レフォール!ほすらていつしゆそす@ホースラディッシュソース（イギリス風）}
\index{sauce@sauce!raifort chaude anglaise@--- au Raifort Chaude (Horse radish Sauce)}
\index{raifort@raifort!sauce anglaise chaude@Sauce au Raifort chaude (Horse radish Sauce)}
\index{anglais@anglais(e)!sauce raifort chaude@Sauce au Raifort chaude (Horse radish Sauce)}
\index[src]{raifort chaude@Raifort chaude (Horese radhish Sauce)}
\index[src]{ほすらていしゆ@ホースラディッシュ（イギリス風）}




⇒ [アルバートソース](#albert-sauce)の別名。









\atoaki{}

#### リフォームソース[^0104_48] {#reform-sauce}

<div class="frsubenv">Sauce Réforme\hspace{1em}\normalfont(Reform Sauce)</div>

\index{いきりすふう@イギリス風!りふおむそす@リフォームソース}
\index{そす@ソース!りふおむそす@リフォームソース}
\index{りふおむ@リフォーム!りふおむそす@リフォームソース（イギリス風）}
\index{sauce@sauce!reforme anglaise@--- Réforme (Reform Sauce)}
\index{reform@reform!sauce anglaise@Sauce Réforme (Reform Sauce)}
\index{anglais@anglais(e)!sauce reform@Sauce Réforme (Reform Sauce)}
\index[src]{reform@Reform}
\index[src]{りふおむ@リフォーム（イギリス風）}





[^0104_48]: 19世紀ロンドンの会員制クラブ、リフォームでフランス人料理長アレ
    クシス・ソワイエが考案したソース。このような場合、Reformを固有名詞
    扱いとして英語のままとするのが現代のフランス語における考え方だが、
    20世紀初頭にはまだ、固有名詞さえもフランス語的に言い換えることがご
    く普通であった。


[ソース・ポワヴラード](#sauce-poivrade-ordinaire)と[ソース・ドゥミグラ
ス](#sauce-demi-glace)を合わせ、ガルニチュールとして 1〜2 mm の細さで短
かめの[ジュリエンヌ\*](#julienne-gls)にした中位のサイズのコルニション 2 個、
固茹で卵の白身、中位の大きさのマッシュルーム 2 個、トリュフ 20 g および[ロ
ングエカルラット\*](#langue-ecarlate-gls)を加える。

……このソースは[リフォーム風](#cotelette-a-la-reforme)羊の[コトレット\*](#cotelette-gls)用。








\atoaki{}

#### セージと玉ねぎのソース {#sage-and-onions-sauce}

<div class="frsubenv">Sauce Sauge et Oignons\hspace{1em}\normalfont(Sage and onions Sauce)</div>
\index{いきりすふう@イギリス風!せしとたまねきのそす@セージと玉ねぎのソース}
\index{そす@ソース!せしとたまねきのそす@セージと玉ねぎのソース}
\index{たまねき@玉ねぎ!せしそすいきりすふう@セージと玉ねぎのソース（イギリス風）}
\index{せし@セージ!たまねきのそすいきりすふう@セージと玉ねぎのソース（イギリス風）}
\index{sauce@sauce!sauge oignons anglaise@--- Sauge et Oignons (Sage and onions Sauce)}
\index{oignon@oignon!sauce sauge anglaise@Sauce Sauge et ---s (Sage and onions Sauce)}
\index{sauge@sauge!sauce oignons anglaise@Sauce --- et Oignons (Sage and onions Sauce)}
\index{anglais@anglais(e)!sauce sauge oignons@Sauce Sauge et Oignons (Sage and onions Sauce)}
\index[src]{sauge oignons anglaise@Sauge et Oignons (Sage and onions Sauce)}
\index[src]{せしとたまねき@セージと玉ねぎ（イギリス風）}



大きめの玉ねぎ2個をオーブンで焼く。冷めたら皮を剥き、[アシェ\*](#hacher-gls)する。パンの身 150 gを牛乳に浸してから圧しつぶして水分を抜く。これと玉ねぎを混ぜあわせる。

セージの[アシェ\*](#hacher-gls)大さじ 2 杯と塩、こしょうで調味する。

……これは鴨の詰め物にする。

###### 【原注】 {#nota-sage-and-onions-sauce}

鴨をローストした際のジュを大さじ 5〜6 杯この詰め物に加えてソース入れで供する。

パンの身と同量の牛の脂身を茹でてから[アシェ\*](#hacher-gls)して加えることも多い。









\atoaki{}

#### ヨークシャーソース[^0104_53] {#sauce-yorkshire}

<div class="frsubenv">Sauce Yorkshire</div>

\index{いきりすふう@イギリス風!よくしやそす@ヨークシャーソース}
\index{そす@ソース!よくしやそす@ヨークシャーソース}
\index{よくしや@ヨークシャー!よくしやそす@ヨークシャーソース（イギリス風）}
\index{sauce@sauce!yorkshire@--- Yorkshire}
\index{yorkshire@Yorkshire!sauce anglaise@Sauce ---}
\index{anglais@anglais(e)!sauce yorkshire@Sauce Yorkshire}
\index[src]{yorkshire@Yorkshire}
\index[src]{よくしや@ヨークシャー（イギリス風）}


オレンジの外皮の硬い表面だけを薄く削って細かい[ジュリエンヌ\*](#julienne-gls)にしたもの大さじ 1
杯強を、ポルト酒 2 dLでしっかり茹でる。

オレンジの皮のジュリエンヌを取り出して水気をきる。ポルト酒の入った鍋に、[ソー
ス・エスパニョル](#sauce-espagnole)大さじ 1 杯強と、[グロゼイユのジュ
レ](#gelee-de-groseilles-procede-a)も大さじ 1 杯強を加える。粉末のシナモン少々と、カイエンヌ少々を加
える。

すこし煮詰める。布で漉し、オレンジ1個の搾り汁とジュリエンヌにした果皮
を加えて仕上げる。

……仔鴨のローストやブレゼ、およびハムのブレゼに添える。

[^0104_53]: このレシピは初版からほぼ異同がなく（初版ではソース名が
    Yorkshire Sauceだったことと、「仔鴨とハムに合わせる」だったのが第
    二版で現在とまったく同じになることのみ）、原注もない。1907年版の英
    語版にも掲載されていないが、1903年アメリカ、シカゴで刊行された
    『[スチュワードハンドブッ
    ク](https://archive.org/details/stewardshandbook00whitiala)』のソー
    スの項目のなかに、「ヨークシャーソース……ハム用のオレンジソース。
    エスパニョル、カラント（=グロゼイユ）ゼリー、ポートワイン、オレン
    ジジュース、茹でて千切りにしたオレンジの外皮」(p.434)とあり、エス
    コフィエの『料理の手引き』初版当時には既にアメリカで知られているソー
    スだったのがわかる。<!--ただしイギリスのヨークシャー州とどのような
    関係あるいはソース名の由来があるのかは不明。-->

</div><!--endRecette-->


<!--20190323江畑校正スミ-->

\index{そす@ソース!いきりすふう@イギリス風---|)}
\index{sauce!anglaise|)}

